package printer

import "fmt"

var VersionValue = "dev"

func PrintVersion() {
	fmt.Println(VersionValue)
}
